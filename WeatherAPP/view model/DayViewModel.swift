//
//  DayViewModel.swift
//  WeatherAPP
//
//  Created by Bridge Technology Services on 26/01/19.
//  Copyright © 2019 Bridge Technology Services. All rights reserved.
//

import Foundation
import UIKit

struct DayViewModel {
    let currently: Currently
    var stringDate: Date
    
    init(currently: Currently) {
        self.currently = currently
        self.stringDate = Date(timeIntervalSince1970: Double(currently.time!))
    }
    
    var date: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, MMM d"
        return dateFormatter.string(from: stringDate)
    }
    
    var time: String {
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "HH:mm"
        return timeFormatter.string(from: stringDate)
    }
    
    var summary: String {
        return currently.summary!
    }
    
    var temperature: String {
        var temperature = Double(currently.temperature!)
        
        if UserDefaults.temperatureNotation() == .fahrenheit {
            return String(format: "%.1f °F", temperature)
        } else {
            temperature = temperature.toCelcius()
            return String(format: "%.1f °C", temperature)
        }
        
    }
    
    var windSpeed: String {
        let windSpeed = Double(currently.windSpeed!)
        return String(format: "%.f MPH", windSpeed)
    }
    
    var image: UIImage? {
        return UIImage.imageForIcon(withName: currently.icon!)
    }
    
}
