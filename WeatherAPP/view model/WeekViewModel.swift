//
//  WeekViewModel.swift
//  WeatherAPP
//
//  Created by Bridge Technology Services on 26/01/19.
//  Copyright © 2019 Bridge Technology Services. All rights reserved.
//

import Foundation
import UIKit

struct WeekViewModel {
    let data: Data
    var stringDate: Date
    
    init(data: Data) {
        self.data = data
        self.stringDate = Date(timeIntervalSince1970: Double(data.time!))
    }
    
    var date: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d"
        return dateFormatter.string(from: stringDate)
    }
    
    var day: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: stringDate)
    }
    
    var time: String {
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "HH:mm"
        return timeFormatter.string(from: stringDate)
    }
    
    var summary: String {
        return data.summary!
    }
    
    var temperature: String {
        var temperatureMax = Double(data.temperatureMax!)
        var temperatureMin = Double(data.temperatureMin!)
        
        if UserDefaults.temperatureNotation() == .fahrenheit {
            return String(format: "%.1f °F - %.1f °F", temperatureMin, temperatureMax)
        } else {
            temperatureMax = temperatureMax.toCelcius()
            temperatureMin = temperatureMin.toCelcius()
            return String(format: "%.1f °C - %.1f °C", temperatureMin, temperatureMax)
        }
        
    }
    
    var windSpeed: String {
        let windSpeed = Double(data.windSpeed!)
        return String(format: "%.f MPH", windSpeed)
    }
    
    var image: UIImage? {
        return UIImage.imageForIcon(withName: data.icon!)
    }
    
}
