//
//  DayViewController.swift
//  WeatherAPP
//
//  Created by Bridge Technology Services on 26/01/19.
//  Copyright © 2019 Bridge Technology Services. All rights reserved.
//

import UIKit

class DayViewController: UIViewController {
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    
    var now: Weather? {
        didSet {
            updateView()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    func updateView() {
        if let now = now {
            bindToView(data: now)
        }
    }
    
    func reloadData() {
        updateView()
    }
    
    private func bindToView(data: Weather) {
        let viewModel = DayViewModel.init(currently: data.currently!)
        
        dateLabel.text = viewModel.date
        timeLabel.text = viewModel.time
        temperatureLabel.text = viewModel.temperature
        windSpeedLabel.text = viewModel.windSpeed
        descLabel.text = viewModel.summary
        iconImage.image = viewModel.image
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
