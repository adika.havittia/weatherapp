//
//  WeekViewController.swift
//  WeatherAPP
//
//  Created by Bridge Technology Services on 26/01/19.
//  Copyright © 2019 Bridge Technology Services. All rights reserved.
//

import UIKit

class WeekViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var week: [Data]? {
        didSet {
            updateView()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func reloadData() {
        updateView()
    }
    
    private func updateView() {
        if week != nil {
            tableView.reloadData()
        }
    }
}

extension WeekViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let week = week else { return 0 }
        return week.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "weekCell", for: indexPath) as? DayTableViewCell else { fatalError() }

        if let week = week {
            let viewModel = WeekViewModel.init(data: week[indexPath.row])
            cell.bindView(viewModel: viewModel)
        }
        
        return cell

    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
