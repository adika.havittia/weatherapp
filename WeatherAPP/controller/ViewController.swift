//
//  ViewController.swift
//  WeatherAPP
//
//  Created by Bridge Technology Services on 26/01/19.
//  Copyright © 2019 Bridge Technology Services. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreLocation
import SVProgressHUD
import LocationPickerViewController

enum APPSegue: String {
    case daySegue = "DaySegue"
    case weekSegue = "WeekSegue"
    case settingSegue = "SettingSegue"
}

class ViewController: UIViewController {
    
    @IBOutlet weak var dayView: UIView!
    @IBOutlet weak var weekView: UIView!
    
    private var dayViewController: DayViewController!
    private var weekViewController: WeekViewController!
    
    private var currentLocation: CLLocation? {
        didSet {
            hideLoading()
            fetchWeather()
        }
    }
    
    private lazy var locationManager : CLLocationManager = {
        let locationManager = CLLocationManager()
        
        locationManager.distanceFilter = 1000
        locationManager.desiredAccuracy = 1000
        
        return locationManager
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        requestLocation()
        setupNotificationHandling()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else { return }
        
        switch identifier {
        case APPSegue.daySegue.rawValue:
            guard let destination = segue.destination as? DayViewController else {
                fatalError()
            }
            self.dayViewController = destination
        case APPSegue.weekSegue.rawValue:
            guard let destination = segue.destination as? WeekViewController else {
                fatalError()
            }
            self.weekViewController = destination
            
        case APPSegue.settingSegue.rawValue:
            guard let destination = segue.destination as? SettingViewController else {
                fatalError()
            }

            destination.delegate = self
        default:
            break
        }
    }
    
    
    func fetchWeather() {
        guard let location = currentLocation else {
            return
        }
        
        self.showLoading()
        
        let latitude = location.coordinate.latitude
        let longitude = location.coordinate.longitude
        
        APIClient.fetchWeather(lat: latitude, long: longitude) { (result) in
            switch result {
            case .success(let value):
                let data = JSON(value)
                let weather = Weather.init(data)
                self.dayViewController.now = weather
                self.weekViewController.week = weather.daily?.data
                self.hideLoading()
            case .failure:
                self.hideLoading()
            }
        }
    }
    
    private func requestLocation() {
        showLoading()
        locationManager.delegate = self
        
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            locationManager.requestLocation()
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    private func setupNotificationHandling() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(applicationDidBecomeActive(notification:)), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    @objc func applicationDidBecomeActive(notification: Notification) {
        requestLocation()
    }
    
    @IBAction func ChangeLocationPressed(_ sender: UIBarButtonItem) {
        let locationPicker = LocationPicker()
        
        locationPicker.pickCompletion = { (pickedLocation) in
            
            guard let latitude = pickedLocation.coordinate?.latitude else {
                return
            }
            
            guard let longitude = pickedLocation.coordinate?.longitude else {
                return
            }
            
            self.currentLocation = CLLocation(latitude: latitude, longitude: longitude)
        }
        
        locationPicker.addBarButtons()
        locationPicker.searchDistance = 1000
        
        let navigationController = UINavigationController(rootViewController: locationPicker)
        present(navigationController, animated: true, completion: nil)
        
        
    }
}

extension ViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            manager.requestLocation()
        } else {
            currentLocation = CLLocation(latitude: Defaults.latitude, longitude: Defaults.longitude)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            currentLocation = location
            manager.delegate = nil
            manager.stopUpdatingLocation()
        } else {
            currentLocation = CLLocation(latitude: Defaults.latitude, longitude: Defaults.longitude)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        if currentLocation == nil {
            currentLocation = CLLocation(latitude: Defaults.latitude, longitude: Defaults.longitude)
        }
    }
}

extension ViewController: ViewProtocol {
    func showLoading() {
        hideView(isHide: true)
        SVProgressHUD.show()
        SVProgressHUD.setDefaultMaskType(.clear)
    }
    
    func hideLoading() {
        hideView(isHide: false)
        SVProgressHUD.dismiss()
    }
    
    func hideView (isHide: Bool) {
        dayView.isHidden = isHide
        weekView.isHidden = isHide
    }
}

extension ViewController: SettingControllerDelegate {
    func controllerDidChangeTemperature(controller: SettingViewController) {
//        dismiss(animated: true, completion: nil)
        navigationController?.popViewController(animated: true)
        dayViewController.reloadData()
        weekViewController.reloadData()
    }
}

