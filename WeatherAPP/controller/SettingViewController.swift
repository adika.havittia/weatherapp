//
//  SettingViewController.swift
//  WeatherAPP
//
//  Created by Bridge Technology Services on 27/01/19.
//  Copyright © 2019 Bridge Technology Services. All rights reserved.
//

import UIKit

protocol SettingControllerDelegate {
    func controllerDidChangeTemperature(controller: SettingViewController)
}

class SettingViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var delegate: SettingControllerDelegate?
    
    private enum Section: Int {
        case temperature
        
        var numberOfRows: Int {
            return 2
        }
        
        static var count: Int {
            return (Section.temperature.rawValue + 1)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableView.separatorInset = UIEdgeInsets.zero
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let section = Section(rawValue: section) else { fatalError()}
        return section.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let section = Section(rawValue: indexPath.section) else {
            fatalError("Unexpected Result")}
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "settingCell", for: indexPath)
        
        
        switch section {
        case .temperature:
            cell.textLabel?.text = (indexPath.row == 0) ? "Fahrenheit" : "Celcius"
            
            let temperatureNotation = UserDefaults.temperatureNotation()
            
            if indexPath.row == temperatureNotation.rawValue {
                cell.accessoryType = .checkmark
            } else {
                cell.accessoryType = .none
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let newTemperatureNotation = TemperatureNotation(rawValue: indexPath.row) {
            UserDefaults.setTemperatureNotation(temperatureNotation: newTemperatureNotation)
        }
        
        delegate?.controllerDidChangeTemperature(controller: self)
        
        tableView.reloadSections(IndexSet(integer: indexPath.section), with: .none)
    }
}

