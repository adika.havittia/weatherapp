//
//  Currently.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on December 10, 2018
//
import Foundation
import SwiftyJSON

struct Currently {

	let time: Int?
	let summary: String?
	let icon: String?
	let precipIntensity: Double?
	let precipProbability: Double?
	let precipType: String?
	let temperature: Double?
	let apparentTemperature: Double?
	let dewPoint: Double?
	let humidity: Double?
	let pressure: Double?
	let windSpeed: Double?
	let windGust: Double?
	let windBearing: Int?
	let cloudCover: Double?
	let uvIndex: Int?
	let visibility: Int?
	let ozone: Double?

	init(_ json: JSON) {
		time = json["time"].intValue
		summary = json["summary"].stringValue
		icon = json["icon"].stringValue
		precipIntensity = json["precipIntensity"].doubleValue
		precipProbability = json["precipProbability"].doubleValue
		precipType = json["precipType"].stringValue
		temperature = json["temperature"].doubleValue
		apparentTemperature = json["apparentTemperature"].doubleValue
		dewPoint = json["dewPoint"].doubleValue
		humidity = json["humidity"].doubleValue
		pressure = json["pressure"].doubleValue
		windSpeed = json["windSpeed"].doubleValue
		windGust = json["windGust"].doubleValue
		windBearing = json["windBearing"].intValue
		cloudCover = json["cloudCover"].doubleValue
		uvIndex = json["uvIndex"].intValue
		visibility = json["visibility"].intValue
		ozone = json["ozone"].doubleValue
	}

}