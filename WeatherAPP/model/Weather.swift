//
//  Weather.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on December 10, 2018
//
import Foundation
import SwiftyJSON

struct Weather {

	let latitude: Double?
	let longitude: Double?
	let timezone: String?
	let currently: Currently?
	let hourly: Hourly?
	let daily: Daily?
	let flags: Flags?
	let offset: Int?

	init(_ json: JSON) {
		latitude = json["latitude"].doubleValue
		longitude = json["longitude"].doubleValue
		timezone = json["timezone"].stringValue
		currently = Currently(json["currently"])
		hourly = Hourly(json["hourly"])
		daily = Daily(json["daily"])
		flags = Flags(json["flags"])
		offset = json["offset"].intValue
	}

}