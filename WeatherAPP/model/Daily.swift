//
//  Daily.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on December 10, 2018
//
import Foundation
import SwiftyJSON

struct Daily {

	let summary: String?
	let icon: String?
	let data: [Data]?

	init(_ json: JSON) {
		summary = json["summary"].stringValue
		icon = json["icon"].stringValue
		data = json["data"].arrayValue.map { Data($0) }
	}

}