//
//  Data.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on December 10, 2018
//
import Foundation
import SwiftyJSON

struct Data {

	let time: Int?
	let summary: String?
	let icon: String?
	let sunriseTime: Int?
	let sunsetTime: Int?
	let moonPhase: Double?
	let precipIntensity: Double?
	let precipIntensityMax: Double?
	let precipIntensityMaxTime: Int?
	let precipProbability: Double?
	let precipType: String?
	let temperatureHigh: Double?
	let temperatureHighTime: Int?
	let temperatureLow: Double?
	let temperatureLowTime: Int?
	let apparentTemperatureHigh: Double?
	let apparentTemperatureHighTime: Int?
	let apparentTemperatureLow: Double?
	let apparentTemperatureLowTime: Int?
	let dewPoint: Double?
	let humidity: Double?
	let pressure: Double?
	let windSpeed: Double?
	let windGust: Double?
	let windGustTime: Int?
	let windBearing: Int?
	let cloudCover: Double?
	let uvIndex: Int?
	let uvIndexTime: Int?
	let visibility: Int?
	let ozone: Double?
	let temperatureMin: Double?
	let temperatureMinTime: Int?
	let temperatureMax: Double?
	let temperatureMaxTime: Int?
	let apparentTemperatureMin: Double?
	let apparentTemperatureMinTime: Int?
	let apparentTemperatureMax: Double?
	let apparentTemperatureMaxTime: Int?

	init(_ json: JSON) {
		time = json["time"].intValue
		summary = json["summary"].stringValue
		icon = json["icon"].stringValue
		sunriseTime = json["sunriseTime"].intValue
		sunsetTime = json["sunsetTime"].intValue
		moonPhase = json["moonPhase"].doubleValue
		precipIntensity = json["precipIntensity"].doubleValue
		precipIntensityMax = json["precipIntensityMax"].doubleValue
		precipIntensityMaxTime = json["precipIntensityMaxTime"].intValue
		precipProbability = json["precipProbability"].doubleValue
		precipType = json["precipType"].stringValue
		temperatureHigh = json["temperatureHigh"].doubleValue
		temperatureHighTime = json["temperatureHighTime"].intValue
		temperatureLow = json["temperatureLow"].doubleValue
		temperatureLowTime = json["temperatureLowTime"].intValue
		apparentTemperatureHigh = json["apparentTemperatureHigh"].doubleValue
		apparentTemperatureHighTime = json["apparentTemperatureHighTime"].intValue
		apparentTemperatureLow = json["apparentTemperatureLow"].doubleValue
		apparentTemperatureLowTime = json["apparentTemperatureLowTime"].intValue
		dewPoint = json["dewPoint"].doubleValue
		humidity = json["humidity"].doubleValue
		pressure = json["pressure"].doubleValue
		windSpeed = json["windSpeed"].doubleValue
		windGust = json["windGust"].doubleValue
		windGustTime = json["windGustTime"].intValue
		windBearing = json["windBearing"].intValue
		cloudCover = json["cloudCover"].doubleValue
		uvIndex = json["uvIndex"].intValue
		uvIndexTime = json["uvIndexTime"].intValue
		visibility = json["visibility"].intValue
		ozone = json["ozone"].doubleValue
		temperatureMin = json["temperatureMin"].doubleValue
		temperatureMinTime = json["temperatureMinTime"].intValue
		temperatureMax = json["temperatureMax"].doubleValue
		temperatureMaxTime = json["temperatureMaxTime"].intValue
		apparentTemperatureMin = json["apparentTemperatureMin"].doubleValue
		apparentTemperatureMinTime = json["apparentTemperatureMinTime"].intValue
		apparentTemperatureMax = json["apparentTemperatureMax"].doubleValue
		apparentTemperatureMaxTime = json["apparentTemperatureMaxTime"].intValue
	}

}