//
//  Flags.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on December 10, 2018
//
import Foundation
import SwiftyJSON

struct Flags {

	let sources: [String]?
	let meteoalarmLicense: String?
	let nearestStation: Double?
	let units: String?

	init(_ json: JSON) {
		sources = json["sources"].arrayValue.map { $0.stringValue }
		meteoalarmLicense = json["meteoalarm-license"].stringValue
		nearestStation = json["nearest-station"].doubleValue
		units = json["units"].stringValue
	}

}
