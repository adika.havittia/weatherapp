//
//  Extension.swift
//  WeatherAPP
//
//  Created by Bridge Technology Services on 26/01/19.
//  Copyright © 2019 Bridge Technology Services. All rights reserved.
//

import Foundation
import UIKit

extension Double {
    func toCelcius() -> Double {
        return ((self - 32.0) / 1.8)
    }
}

extension UIImage {
    class func imageForIcon(withName name: String) -> UIImage {
        switch name {
        case Asset.clearDay.name:
            return Asset.clearDay.image
        case Asset.clearNight.name:
            return Asset.clearNight.image
        case Asset.rain.name:
            return Asset.rain.image
        case Asset.snow.name:
            return Asset.snow.image
        case Asset.sleet.name:
            return Asset.sleet.image
        case Asset.cloudy.name:
            return Asset.cloudy.image
        default:
            return Asset.cloudy.image
        }
    }
}

enum TemperatureNotation: Int {
    case fahrenheit
    case celcius
}

struct UserDefaultKeys {
    static let temperatureNotation = "temperatureNotation"
}

extension UserDefaults {
    static func temperatureNotation() -> TemperatureNotation {
        let storedValue = UserDefaults.standard.integer(forKey: UserDefaultKeys.temperatureNotation)
        return TemperatureNotation(rawValue: storedValue) ?? TemperatureNotation.fahrenheit
    }
    
    static func setTemperatureNotation(temperatureNotation: TemperatureNotation) {
        UserDefaults.standard.set(temperatureNotation.rawValue, forKey: UserDefaultKeys.temperatureNotation)
    }
}
