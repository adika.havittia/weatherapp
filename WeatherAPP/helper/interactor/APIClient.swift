//
//  APIClient.swift
//  WeatherAPP
//
//  Created by Bridge Technology Services on 26/01/19.
//  Copyright © 2019 Bridge Technology Services. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class APIClient {
    private static func performRequest(route:APIRouter, completion:@escaping (Result<Any>) -> Void){
        Alamofire.request(route).validate().responseJSON { (response) in
            completion(response.result)
        }
    }
    
    static func fetchWeather(lat:Double, long:Double, completion:@escaping (Result<Any>)->Void) {
        performRequest(route: APIRouter.fetchWeather(lat: lat, long: long), completion: completion)
    }
    
}
