//
//  APIRouter.swift
//  WeatherAPP
//
//  Created by Bridge Technology Services on 26/01/19.
//  Copyright © 2019 Bridge Technology Services. All rights reserved.
//

import Foundation
import Alamofire

enum APIRouter: URLRequestConvertible {
    case fetchWeather(lat: Double, long: Double)
    
    private var method: HTTPMethod {
        switch self {
        case .fetchWeather:
            return .get
        }
    }
    
    private var path: String {
        switch self {
        case .fetchWeather(let lat, let long):
            return "/\(API.apikey)/\(lat),\(long)"
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try API.BaseURL.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.timeoutInterval = 60 // 10 sec
        urlRequest.httpMethod = method.rawValue
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.acceptType.rawValue)
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.contentType.rawValue)
        
        return urlRequest
    }
    
}


