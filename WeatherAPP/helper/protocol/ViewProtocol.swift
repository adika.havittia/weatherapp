//
//  ViewProtocol.swift
//  WeatherAPP
//
//  Created by Bridge Technology Services on 26/01/19.
//  Copyright © 2019 Bridge Technology Services. All rights reserved.
//

import Foundation

protocol ViewProtocol {
    func showLoading()
    func hideLoading()
}
